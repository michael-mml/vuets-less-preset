# vuets-less-preset

This is a repo to share a Vue.js + Vuex + Vue-router + TypeScript + LESS + ESLint preset.

## Getting Started

To create a new Vue project with this [preset](https://cli.vuejs.org/guide/plugins-and-presets.html#remote-presets), run: `vue create --preset gitlab:michael-mml/vuets-less-preset <app_name>`

To upload a new preset, find the preset that was saved during `vue create`. This is typically located at `~/.vuerc`. Extract the preset key-value pair as [follows](https://cli.vuejs.org/guide/plugins-and-presets.html#presets):

```json
{
  "useConfigFiles": true,
  "router": true,
  "vuex": true,
  "cssPreprocessor": "sass",
  "plugins": {
    "@vue/cli-plugin-babel": {},
    "@vue/cli-plugin-eslint": {
      "config": "airbnb",
      "lintOn": ["save", "commit"]
    }
  }
}
```
